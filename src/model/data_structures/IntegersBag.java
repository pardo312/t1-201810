package model.data_structures;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;


public class IntegersBag {
	
	private HashSet<Integer> bag;
	
	public IntegersBag(){
		this.bag = new HashSet<>();
	}
	
	public IntegersBag(ArrayList<Integer> data){
		this();
		if(data != null){
			for (Integer datum : data) {
				bag.add(datum);
			}
		}
		
	}
	public int darCodigo()
	{
		return bag.hashCode();
	}
	
	public int darTamanho()
	{
		return bag.size();
	}
	
	
	public boolean eliminar(int posicion)
	{
		return bag.remove(posicion);
	}
	public void addDatum(Integer datum){
		bag.add(datum);
	}
	
	public Iterator<Integer> getIterator(){
		return this.bag.iterator();
	}

}
